'use strict';

module.exports.lambda3 = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Lambda 3-ok!',    
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};


\/.*

functions/primer-lambda/src/handler.js
functions/segunda-lambda/src/handler.js