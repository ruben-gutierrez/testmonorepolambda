'use strict';

module.exports.lambda4 = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Lambda 4-ok!',    
        input: event,
      },
      null,
      2 
    ),
  }; 

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

