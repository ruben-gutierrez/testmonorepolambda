#!/bin/bash


routeRaiz=$(pwd)
dirLambdas=$(find -depth  -name serverless.yml | sed -e "s/\.\///g" | sed -e "s/\/serverless.yml//g")


changes=$(git diff --name-status HEAD^ HEAD | grep '/' | awk '{ print $2 }'  | xargs dirname | sort | uniq)
if [ $# -ge 1 ];
then
    for nameFile in $@
    do
        excludeFiles=$excludeFiles" -e "$nameFile
    done
    changes=$(git diff --name-status HEAD^ HEAD | grep '/' | awk '{ print $2 }' | xargs dirname | egrep -v $excludeFiles | sort | uniq)
fi

for lambda in $dirLambdas
do
    for change in $changes
    do
        if echo $change | grep -q $lambda; then
            cd ./$lambda
            serverless deploy
            cd $routeRaiz
            break
        fi
    done
done



